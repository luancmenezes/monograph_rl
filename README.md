# VINCULAÇÃO DE BASES ACADÊMICAS
## O CASO UFBADB E LDAP
Código utilizado para a realização da vinculação das bases.

Foi foi utilizado:  

||
|-|
|[Python 2.7](https://www.python.org/)|
|[Spark](https://spark.apache.org/) |
|[Jellyfish](https://pypi.org/project/jellyfish/) |
